INTRODUCTION
============
Adds in the Xml plugin for CKEditor.

This is required by plugins that need xml plugin to work, like Emoji.

REQUIREMENTS
============
This module requires the core CKEDITOR module.

CONFIGURATION
============
Other plugins can leverage xml by adding this in their plugin definition:

```
class YourButtonPlugin extends CKEditorPluginBase 
implements CKEditorPluginConfigurableInterface {
  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return ['xml'];
  }

  // more code...
```
INSTALLATION
============

1.Download the library from https://ckeditor.com/cke4/addon/xml

2.Place the library in the root libraries folder (/libraries).

3.Enable Xml module.

LIBRARY INSTALLATION (COMPOSER)
-------------------------------
1.Copy the following into your project's composer.json file.

```
"repositories": [
  "ckeditor-plugin/xml": {
    "type": "package",
    "package": {
      "name": "ckeditor-plugin/xml",
      "version": "4.11.4",
      "type": "drupal-library",
      "dist": {
        "url": "https://download.ckeditor.com/xml/releases/xml_4.11.4.zip",
        "type": "zip"
      }
    }
  }
]
```
2.Ensure you have following mapping inside your composer.json.
```
"extra": {
  "installer-paths": {
    "libraries/{$name}": ["type:drupal-library"]
  }
}
```
3.Run following command to download required library.
```
composer require ckeditor-plugin/xml
```
4.Enable the Xml module.
